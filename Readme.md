# Running the demo

I would make a snarky comment about trivial exercises left up to the reader,
but that would be evil. Don't do this, don't be evil.

I am not great at assembly, or configuring my linker (ld65), and this is not
a single, self-contained binary.

To view the complete effect, htimg.bin must be loaded into three places in
memory using BLOAD.

```
BLOAD HTIMG.BIN,$A2000
BLOAD HTIMG.BIN,A$4004
BLOAD HTIMG.BIN,A$6000
BRUN DEMO.BIN
```

The graphics data at $6000 is a cache for real-time effects. The memory 
addresses $2000 and $4000 are the high-res graphics mode page 1 and page 2
buffers. The data is loaded into $4004 to fix a byte offset error. This is
intended, because, the same data loaded into $2000 causes an intentional
rendering issue.

If I had spent more time using the indirect indexed addressing mode in more
places I could minimize this to a single BLOAD, but I haven't improved it in
this way yet. If anyone can help me configure ld65 to include the $2000 bytes
of graphics data directly in the executable, I would be very grateful.
AppleDOS throws I/O errors when I have tried.

I'll upload the binaries as well, in case people don't want to assemble their
own. I ran this on the Virtual ][ emulator, although I needed to configure the
file as "binary, strip prefix $8000" for it to run properly. Your emulator may
not have this feature, it hasn't been tested on real hardware, and this may
be a quirk of me not knowing what I'm doing. :)
